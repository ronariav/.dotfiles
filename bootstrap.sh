#!/usr/bin/env bash

function pause() {
  read -n 1 -p "Click any key to continue..." -s -e -r
}

function install_brew() {
  echo "Installing Homebrew..."

  /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
	# Add brew to PATH
  echo 'eval "$(/opt/homebrew/bin/brew shellenv)"' >> /Users/$(basename ~)/.zprofile
  eval "$(/opt/homebrew/bin/brew shellenv)"
}

function generate_ssh_key() {
  echo "Generating SSH key..."
  read -r -p "Enter your email: " email
  ssh-keygen -t ed25519 -C \""$email"\"

  echo "Copying public key to clipboard"
  pbcopy <"$HOME"/.ssh/id_ed25519.pub

  echo "Please upload to GitLab"
  pause

  echo "Starting ssh-agent"
  eval "$(ssh-agent -s)"

  echo "Adding ssh keys to Keychain"
  cat <<EOF >"$HOME"/.ssh/config
Host *
AddKeysToAgent yes
UseKeychain yes
IdentityFile ~/.ssh/id_ed25519
EOF

  echo "Adding key to agent"
  ssh-add -K "$HOME"/.ssh/id_ed25519
}

function configure() {
    echo "Moving to $HOME"
    cd "$HOME" || exit

    echo "Cloning .dotfiles"
    git clone git@gitlab.com:ronariav/.dotfiles.git

    echo "Moving to $HOME/.dotfiles"
    cd .dotfiles || exit

    echo "Running the install script"
    source install.sh
		start
}

function main() {
  install_brew
  generate_ssh_key
  configure

  echo "🍻 Success!"
}

main
