```
         __      __  _______ __         
    ____/ /___  / /_/ ____(_) /__  _____
   / __  / __ \/ __/ /_  / / / _ \/ ___/
 _/ /_/ / /_/ / /_/ __/ / / /  __(__  ) 
(_)__,_/\____/\__/_/   /_/_/\___/____/  
                                        
```
## Install

Clone this repository into your home directory.

```shell
$ cd ~
$ git clone git@gitlab.com:ronariav/dotfiles.git 
```

To bootstrap a new machine, run the `install` script.

```shell
$ ~/.dotfiles/install
```

To only install the dotfiles, run the `rcup` script.

```shell
$ ~/.dotfiles/rcup
```

## Bootstrap

The curl flags are copied from the Homebrew install method

```bash
/bin/bash -c "$(curl -fsSL https://gitlab.com/ronariav/dotfiles/-/raw/main/bootstrap.sh)"
```
