-- :help options
local options  = {
	splitbelow = true,    -- force all horizontal splits to go below current window
	splitright = true,    -- force all vertical splits to go to the right of current window
	tabstop = 2,          -- insert two spaces for a tab
	shiftwidth = 2,       -- the number of spaces inserted for each indentation
	number = true,        -- set numbered lines
	numberwidth = 2,      -- set number column width to 2 (default is 4) 
	termguicolors = true, -- set term gui colors
}

for k, v in pairs(options) do
		vim.opt[k] = v
end
