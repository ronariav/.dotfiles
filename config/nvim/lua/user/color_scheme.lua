local color_scheme = "onedark"

local status_ok, _ = pcall(vim.cmd, "colorscheme " .. color_scheme)
if not status_ok then
	vim.notify("colorscheme " .. color_scheme .. " not found!")
	return
end
