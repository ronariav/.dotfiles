#! /usr/bin/env bash

install_brew_dependencies() {
  echo "==> Install Homebrew dependencies"
  brew bundle
}

update_shell() {
  echo "==> Changing your shell to fish..."
  echo /opt/homebrew/bin/fish | sudo tee -a /etc/shells
  chsh -s /opt/homebrew/bin/fish
}

configure_dock() {
  defaults write com.apple.dock orientation left; defaults write com.apple.dock autohide -bool TRUE; defaults write com.apple.dock autohide-time-modifier -float 0.2; killall Dock
}

configure_app_store_apps() {
  dockutil --remove 'Safari'
  dockutil --remove 'Contacts'
  dockutil --remove 'Notes'
  dockutil --remove 'Music'
  dockutil --remove 'Reminders'
  dockutil --remove 'News'
  dockutil --remove 'Keynote'
  dockutil --remove 'Pages'
  dockutil --remove 'TV'
  dockutil --remove 'Podcasts'
  dockutil --remove 'Numbers'
  dockutil --add '/Applications/Bear.app'
  dockutil --add '/Applications/Firefox.app'
  dockutil --add '/Applications/iTerm.app'
}

link_dot_files() {
  cd .dotfiles || exit
  source rcup
}

install_languages() {
  echo "==> Installing languages"
  for plugin in erlang elixir nodejs ruby; do
    echo "==> Installing asdf plugin: $plugin"
    asdf plugin add "$plugin"
  done

  (cd "$HOME" && asdf install)
}

start() {
  install_brew_dependencies
  update_shell
  link_dot_files
  configure_app_store_apps
  configure_dock
  install_languages      
}
